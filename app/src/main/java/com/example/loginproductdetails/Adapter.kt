package com.example.loginproductdetails

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class Adapter(val context: Context, val data: List<Passengers>): RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflator = LayoutInflater.from(parent.context)
        val view = inflator.inflate(R.layout.list_view, parent,false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.txtTile.text = item.name
        Glide.with(context).load(item.airline[0].logo).into(holder.articleImg);
        holder.itemContainer.setOnClickListener {
            val activity = Intent(context, DetailActivity::class.java)
            activity.putExtra(context.resources.getString(R.string.passengerId),item._id)
            activity.putExtra(context.resources.getString(R.string.passengerName),item.name)
            activity.putExtra(context.resources.getString(R.string.airlineLogo),item.airline[0].logo)
            context.startActivity(activity)
        }
    }

    override fun getItemCount(): Int {
        return data.size;
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtTile = itemView.findViewById<TextView>(R.id.txtTitle);
        val articleImg = itemView.findViewById<ImageView>(R.id.articleImg);
        val itemContainer = itemView.findViewById<LinearLayout>(R.id.itemContainer);
    }
}