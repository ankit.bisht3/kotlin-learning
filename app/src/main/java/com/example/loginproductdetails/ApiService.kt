package com.example.loginproductdetails

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val BASE_URL = "https://api.instantwebtools.net/v1/";
interface ApiInterface {

    @GET("passenger")
    fun getAirlines(@Query("page") page: Int = 1, @Query("size") size: Int = 20) : Call<ApiMain>

    // creating singleton object
    object ApiService {
        val apiInstance: ApiInterface;
        init {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
            apiInstance = retrofit.create(ApiInterface::class.java)
        }
    }
}