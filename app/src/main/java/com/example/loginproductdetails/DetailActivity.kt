package com.example.loginproductdetails

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val extras = intent.extras
        if (extras != null) {
            val id = extras.getString(resources.getString(R.string.passengerId))
            val name = extras.getString(resources.getString(R.string.passengerName))
            val logo = extras.getString(resources.getString(R.string.airlineLogo))
            val passengerName = findViewById<TextView>(R.id.passengerName) as TextView
            val passengerId = findViewById<TextView>(R.id.passengerId) as TextView
            val airlineLogo = findViewById<ImageView>(R.id.airlineLogo) as ImageView
            Glide.with(this).load(logo).into(airlineLogo);
            passengerName.append(" $name")
            passengerId.append(" $id")
        }
    }
}