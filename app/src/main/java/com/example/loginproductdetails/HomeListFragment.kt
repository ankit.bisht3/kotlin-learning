package com.example.loginproductdetails

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeListFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAirlinesData(view)
    }

    private fun getAirlinesData(view: View) {
        val airlines = ApiInterface.ApiService.apiInstance.getAirlines();
        airlines.enqueue(object : Callback<ApiMain> {
            override fun onResponse(call: Call<ApiMain>, response: Response<ApiMain>) {
                val res = response.body()
                if(res != null){
                    val adapter = Adapter(view.context, res.data)
                    val passengerList = view?.findViewById(R.id.listItem) as RecyclerView
                    passengerList.adapter = adapter
                    passengerList.layoutManager = LinearLayoutManager(view.context)
                }
            }

            override fun onFailure(call: Call<ApiMain>, t: Throwable) {
                Log.d("Api Error", t.toString())
            }

        } )
    }
}