package com.example.loginproductdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState);
        renderLoginPage();
    }

    private fun renderLoginPage() {
        val loginImg = view?.findViewById(R.id.appLogo) as ImageView;
        Glide.with(this).load(R.drawable.app_logo).into(loginImg);

        val loginBtn = view?.findViewById(R.id.loginBtn) as Button;
        val mobileInput = view?.findViewById(R.id.mobileInput) as TextView;
        val passwordInput = view?.findViewById(R.id.passwordInput) as TextView;

        loginBtn.setOnClickListener {
            if((mobileInput.text.toString().isEmpty() && passwordInput.text.toString().isEmpty()) || mobileInput.text.toString().length < 10){
                Toast.makeText(activity,resources.getString(R.string.mobileCantBeEmpty), Toast.LENGTH_SHORT).show();
            } else {
                findNavController().navigate(R.id.action_loginFragment2_to_homeList)
            }
        }
    }

}