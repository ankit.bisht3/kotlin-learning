package com.example.loginproductdetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.login)

        var navHostFragment = supportFragmentManager
            .findFragmentById(R.id.loginFragmentContainerView) as NavHostFragment;
        navController = navHostFragment.navController
    }
}
