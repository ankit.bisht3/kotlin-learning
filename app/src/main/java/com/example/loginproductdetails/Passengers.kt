package com.example.loginproductdetails

data class Passengers(
    val _id: String,
    val name: String,
    val airline: List<Airlines>
    ) {}